package com.github.libretube.obj

data class NavBarItem(
    val id: Int = 0,
    val titleResource: Int = 0,
    var isEnabled: Boolean = true
)
